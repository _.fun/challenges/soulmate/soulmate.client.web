import styled, { css } from '@/styled-components'

import { InformationLayout } from './information'
import { ListLayout } from './list'

interface ILayoutProperties {
  isCustomerSelected: boolean
}

export const Layout = styled.div<ILayoutProperties>`
  ${({ isCustomerSelected }) => css`
    display: grid;

    grid-template-areas: 
      'title title'
      'list information'
    ;
    grid-template-columns: 1fr 1fr;

    @media (max-width: 768px) {
      grid-template-areas: 
        'title'
        '${isCustomerSelected ? 'information' : 'list'}'
      ;
      grid-template-columns: 1fr;

      > ${isCustomerSelected ? ListLayout : InformationLayout} {
        display: none;
      }
    }
  `}
`

export const Title = styled.div`
  grid-area: title;
`
