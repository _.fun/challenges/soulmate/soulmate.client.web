import { useState } from 'react'

import {
  ICustomerModel,
  IFeedbackModel,
} from '@/services/customers'

interface IState {
  customers: ICustomerModel[]
  selectedCustomer?: ICustomerModel
  addCustomer: (name: string) => void
  addFeedback: (text: string) => void
}

interface ICustomerStateParameters {
  customerId: number
}

// does to much
export const useCustomersState = ({ customerId }: ICustomerStateParameters): IState => {
  const [customers, setCustomers] = useState<ICustomerModel[]>([])

  const selectedCustomer = customers.find(x => x.id === customerId)

  // NOTE: would've been easier to get data from api
  const addCustomer = (name?: string): void => {
    if (!name) return

    const customer: ICustomerModel = {
      feedbacks: [],
      id: customers.length === 0 ? 1 : customers[customers.length-1].id + 1,
      name,
    }

    setCustomers([
      ...customers,
      customer,
    ])
  }

  const addFeedback = (text?: string): void => {
    if (!selectedCustomer || !text) return

    const { feedbacks } = selectedCustomer
    const feedback: IFeedbackModel = {
      id: feedbacks.length === 0 ? 1 : feedbacks[feedbacks.length-1].id + 1,
      text,
    }
    const customer: ICustomerModel = {
      ...selectedCustomer,
      feedbacks: [
        ...feedbacks,
        feedback,
      ],
    }

    setCustomers([
      ...customers.filter(x => x.id !== selectedCustomer.id),
      customer
    ])
  }

  return {
    addCustomer,
    addFeedback,
    customers,
    selectedCustomer,
  }
}
