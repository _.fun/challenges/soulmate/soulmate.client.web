import React, { FunctionComponent } from 'react'

import { ItemLayout } from '@/components/layout'

import { IFeedbackModel } from '@/services/customers'

interface IItemProperties {
  item: IFeedbackModel
}

export const Item: FunctionComponent<IItemProperties> = ({ item }) => (
  <ItemLayout>
    {item.text}
  </ItemLayout>
)
