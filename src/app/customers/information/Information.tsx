import React, { FunctionComponent } from 'react'

import { ICustomerModel } from '@/services/customers'

import { ListLayout } from '@/components/layout'
import { HeaderH2 } from '@/components/typography'

import { CloseButton, Layout, Title } from './styled'

import { Empty } from './Empty'
import { Item } from './item'

interface IInformationProperties {
  customer?: ICustomerModel
  onClose: () => void
}

export const Information: FunctionComponent<IInformationProperties> = ({
  customer,
  children,
  onClose,
}) => {
  if (!customer) return <Empty />

  return (
    <Layout>
      <Title>
        <HeaderH2>Feedbacks ({customer.name})</HeaderH2>
        <CloseButton onClick={onClose}>close</CloseButton>
      </Title>

      {children}

      <ListLayout>
        {customer.feedbacks.map(x => <Item key={x.id} item={x} />)}
      </ListLayout>
    </Layout>
  )
}