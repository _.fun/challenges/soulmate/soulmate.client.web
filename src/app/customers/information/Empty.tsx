import React, { FunctionComponent } from 'react'

import { HeaderH2 } from '@/components/typography'

import { EmptyBlock, LayoutEmpty, Title } from './styled'

export const Empty: FunctionComponent = () => (
  <LayoutEmpty>
    <Title>
      <HeaderH2>Feedbacks</HeaderH2>
    </Title>
    <EmptyBlock>Please select customer</EmptyBlock>
  </LayoutEmpty>
)
