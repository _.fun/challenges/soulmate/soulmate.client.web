import { ChangeEvent, useState } from 'react'

interface ISwitchState {
  isAdding: boolean
  turnOff: () => void
  turnOn: () => void
}


export const useSwitchState = (): ISwitchState => {
  const [isAdding, setIsAdding] = useState<boolean>(false)

  const turnOff = (): void => setIsAdding(false)
  const turnOn = (): void => setIsAdding(true)

  return {
    isAdding,
    turnOff,
    turnOn,
  }
}

interface IAddState {
  name: string
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
  onSubmit: (e: ChangeEvent<HTMLFormElement>) => void
}

export interface IAddStateParameters {
  onAdd: (name: string) => void
  onAction: () => void
}

export const useAddState = ({ onAdd, onAction }: IAddStateParameters): IAddState => {
  const [name,  setName] = useState<string>('')

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.currentTarget.value)
  }
  const onSubmit = (e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault()
    onAdd(name)
    onAction()
    setName('')
  }

  return {
    name,
    onChange,
    onSubmit,
  }
}
