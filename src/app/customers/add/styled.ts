import styled, { css } from '@/styled-components'

import { ItemLayout } from '@/components/layout'

export const Layout = styled(ItemLayout)`
  text-align: center;
`

export const Title = styled.div``

export const Form = styled.form`
  ${({ theme: { spaces } }) => css`
    display: grid;
    grid-gap: ${spaces.x1};
  `}
`

export const Input = styled.input``

export const Button = styled.button``
