import { act, renderHook } from '@testing-library/react-hooks'

import { useSwitchState } from '../useAddState'

describe('add.useSwitchState()', () => {
  it('should have default state', () => {
    const { result } = renderHook(() => useSwitchState())

    expect(result.current.isAdding).toEqual(false)
  })

  it('should have been turned on', () => {
    const { result } = renderHook(() => useSwitchState())

    act(() => result.current.turnOn())

    expect(result.current.isAdding).toEqual(true)
  })

  it('should have been turned off', () => {
    const { result } = renderHook(() => useSwitchState())

    act(() => result.current.turnOn())
    act(() => result.current.turnOff())

    expect(result.current.isAdding).toEqual(false)
  })
})