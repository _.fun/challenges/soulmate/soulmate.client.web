import styled, { css } from '@/styled-components'

export const ListLayout = styled.div`
${({ theme: { spaces } }) => css`
  display: grid;
  grid-auto-flow: row;
  grid-gap: ${spaces.x1};
`}
`